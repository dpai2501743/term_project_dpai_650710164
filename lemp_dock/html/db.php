<!DOCTYPE html>
<html lang="th">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
    font-family: Arial, sans-serif;
    background: linear-gradient(to right, #78A3D4, #8BADD3, #93B9DD, #A6C3E4, #CfE0EB, #9DCAEB, #AFD5F0, #C3EEFA, #DAFAFA);
    text-align: center;
    margin: 0;
    padding: 0;
}

header {
    background-color: #78A3D4;
    color: #fff;
    padding: 10px 0;
}

nav ul {
    list-style: none;
    padding: 0;
}

nav li {
    display: inline;
    margin-right: 20px;
}

.button {
    background-color: #9DCAEB;
    color: white;
    padding: 10px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    border-radius: 5px;
}

</style>
</head>
<body>
    <header>
        <nav>
            <ul>
                <li><a class="button" href="index.html">PROFILE</a></li>
                <li><a class="button" href="interested.html">สิ่งที่ฉันสนใจ</a></li>
                <li><a class="button" href="about_su.html">รอบรั้วศิลปากร</a></li>
                <li><a class="button" href="db.php">การเชื่อมต่อกับ Database</a></li>
            </ul>
        </nav>
    </header>
</body>
</html>

<?php
   $servername = "db";
   $username = "devops";
   $password = "devops101";

   $dbhandle = mysqli_connect($servername, $username, $password);
   $selected = mysqli_select_db($dbhandle, "titanic");
   
   echo "Connected database server<br>";
   echo "Selected database";
?>

